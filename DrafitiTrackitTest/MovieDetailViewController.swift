//
//  MovieDetailViewController.swift
//  DrafitiTrackitTest
//
//  Created by Leon Berni on 11/11/17.
//  Copyright © 2017 Leon Berni. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {
    @IBOutlet weak var movieNamelbl: UILabel!
    @IBOutlet weak var releaseDatelbl: UILabel!
    @IBOutlet weak var runtimelbl: UILabel!
    @IBOutlet weak var taglinelbl: UILabel!
    @IBOutlet weak var ratinglbl: UILabel!
    @IBOutlet weak var genrelbl: UILabel!
    @IBOutlet weak var overviewTxtview: UITextView!
    @IBOutlet weak var moviePosterImg: UIImageView!
    var posterImage: UIImage?
    

    var selectedMovieDetail: MovieModel = MovieModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.moviePosterImg.image = posterImage
        self.movieNamelbl.text = self.selectedMovieDetail.title
        self.releaseDatelbl.text?.append(String(format: ": %@",self.selectedMovieDetail.released!))
        self.ratinglbl.text?.append(String(format: ": %.2f",Float(self.selectedMovieDetail.rating!)!))
        self.runtimelbl.text?.append(String(format: ": %@ days",self.selectedMovieDetail.runtime!))
        self.overviewTxtview.text = self.selectedMovieDetail.overview
        self.taglinelbl.text?.append(String(format: ": %@",self.selectedMovieDetail.tagline!))
        self.genrelbl.text?.append(": ");
        for genre in self.selectedMovieDetail.genres {
            self.genrelbl.text?.append(genre!);
            self.genrelbl.text?.append(", ");
        }
        let substring = self.genrelbl.text![(self.genrelbl.text?.startIndex)!..<(self.genrelbl.text?.index((self.genrelbl.text?.endIndex)!, offsetBy: -2))!]
        self.genrelbl.text = String(substring)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
