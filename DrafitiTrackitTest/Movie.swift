//
//  MovieModel.swift
//  DrafitiTrackitTest
//
//  Created by Leon Berni on 10/11/17.
//  Copyright © 2017 Leon Berni. All rights reserved.
//

import UIKit
import SwiftyJSON

class MovieModel: NSObject {
    var title: String? = ""
    var year: String? = ""
    var tagline: String? = ""
    var released: String? = ""
    var rating: String? = ""
    var runtime: String? = ""
    var overview: String? = ""
    var genres: [String?] = [""]
    var movieID: String = ""
    var imagePath: String? = ""
    
    override init() {
        super.init()
    }
    
    init(json: JSON) {
        self.title = json["movie"]["title"].stringValue
        self.year = json["movie"]["year"].stringValue
        self.rating = json["movie"]["rating"].stringValue
        self.genres = json["movie"]["genres"].arrayObject as! [String]
        self.released = json["released"].stringValue
        self.tagline = json["movie"]["tagline"].stringValue
        self.overview = json["movie"]["overview"].stringValue
        self.runtime = json["movie"]["runtime"].stringValue
        self.movieID = json["movie"]["ids"]["imdb"].stringValue
    }
    
}
