//
//  APICaller.swift
//  DrafitiTrackitTest
//
//  Created by Leon Berni on 10/11/17.
//  Copyright © 2017 Leon Berni. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON

class APICaller: NSObject {
    let header : [String : String] = [
        "Content-Type": "application/json",
        "trakt-api-version" : "2",
        "trakt-api-key": "ba5846a2450f457a1447b9b3161fcb8566c82c91cd6b4bcf3f584b3f7f88f630"
    ]
    
    func getMovies(completion: @escaping (JSON) -> ()) -> () {
        let url = "https://api.trakt.tv/calendars/all/movies/?extended=full"
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.header).responseJSON{
            response in
            
            switch response.result {
                
            case .success:
                let swiftyJsonVar = JSON(response.result.value!)
                completion(swiftyJsonVar)
                break
            case .failure:
                print("Failed getting data")
                completion(JSON())
                break
            }
        }
    }
    
    func getMoviePoster(id: String?, cell: MovieCatalogTableViewCell) -> () {
        let url = String(format: "https://api.themoviedb.org/3/find/%@?api_key=01e487c1e66af21b6e6a4208e7483d11&language=en-US&external_source=imdb_id",id!)
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.header).responseJSON{
            response in
            
            switch response.result {
                
            case .success:
                let swiftyJsonVar = JSON(response.result.value!)
                let imgUrl = String(format: "http://image.tmdb.org/t/p/w500%@", swiftyJsonVar["movie_results"][0]["poster_path"].stringValue)
                Alamofire.request(imgUrl).responseImage { response in
                    if let image = response.result.value {
                        cell.moviePosterimg.image = image
                    }
                }
            case .failure:
                print("Failed getting data")
            }
        }
    }
}
