//
//  MovieCatalogTableViewCell.swift
//  DrafitiTrackitTest
//
//  Created by Leon Berni on 10/11/17.
//  Copyright © 2017 Leon Berni. All rights reserved.
//

import UIKit

class MovieCatalogTableViewCell: UITableViewCell {
    @IBOutlet weak var movieNamelbl: UILabel!
    @IBOutlet weak var movieYearlbl: UILabel!
    @IBOutlet weak var moviePosterimg: UIImageView!
    var movieInfo: MovieModel = MovieModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func prepareForReuse() {
        self.moviePosterimg.image = UIImage(named: "icon")
    }
}
