//
//  ViewController.swift
//  DrafitiTrackitTest
//
//  Created by Leon Berni on 10/11/17.
//  Copyright © 2017 Leon Berni. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var movieJson: JSON?
    var movies: [MovieModel]? = [MovieModel]()
    var filteredSearch: [MovieModel]? = [MovieModel]()
    let apiCallerInstance = APICaller.init()
    var selectedMovie = MovieModel()
    var selectedMovieposter: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshList(self)
    }
    
    @IBAction func refreshList(_ sender: Any) {
        self.apiCallerInstance.getMovies(completion: { movieJson in
            self.movieJson = movieJson;
            for movie in self.movieJson! {
                let movieToAdd = MovieModel(json: movie.1)
                self.movies?.append(movieToAdd)
            }
            self.filteredSearch = self.movies!
            self.tableView.reloadData()
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! MovieDetailViewController
        vc.selectedMovieDetail = selectedMovie
        vc.posterImage = self.selectedMovieposter
    }
    func storeMovieDetails(movie: MovieModel, cell: MovieCatalogTableViewCell) -> () {
        cell.movieInfo = movie
    }
}


extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = self.tableView.cellForRow(at: indexPath) as! MovieCatalogTableViewCell
        self.selectedMovie = cell.movieInfo
        self.selectedMovieposter = cell.moviePosterimg.image
        self.performSegue(withIdentifier: "DetailSegue", sender: self);
        
    }
}


extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard self.filteredSearch != nil else { return 0 }
        return self.filteredSearch!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "MovieCell")! as! MovieCatalogTableViewCell
        cell.movieNamelbl.text = self.filteredSearch![indexPath.row].title
        cell.movieYearlbl.text = String(format: "Released: %@", self.filteredSearch![indexPath.row].released!)
        storeMovieDetails(movie: self.filteredSearch![indexPath.row], cell: cell)
        self.apiCallerInstance.getMoviePoster(id: cell.movieInfo.movieID, cell: cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.text = ""
        self.filteredSearch = self.movies!
        self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.filteredSearch?.removeAll()
        for movie in self.movies! {
            if (movie.title != nil && (movie.title?.contains(find: searchBar.text!))!) {
                self.filteredSearch?.append(movie)
            }
        }
        self.tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            self.filteredSearch = self.movies!
            self.tableView.reloadData()
        }
    }
    
    
}

extension String {
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}
